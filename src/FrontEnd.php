<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\DriverManager;



class FrontEnd
{
	//home
	public function indexAction(Application $app) {
    	return '';
			
	}

	//funcion para el registro de los clientes
	public function registro(Application $app)    {
		
	$clave = 'Clave';
	$nombre = 'Nombre';
	$apellido = 'Apellido';
	$correo = 'Correo';
	$calle = 'Calle';
	$colonia = 'Colonia';
	$cp = 'CP';
	$ciudad = 'Ciudad';
	$pais = 'Pais';
	$telefono = 'Telefono';

	return $app['twig']->render('registro.twig', array(
		'clave' => $clave,
		'nombre' => $nombre,
		'apellido' => $apellido,
		'correo' => $correo,
		'calle' => $calle,
		'colonia' => $colonia,
		'cp' => $cp,
		'ciudad' => $ciudad,
		'pais' => $pais,
		'telefono' => $telefono
		));
		
	}
	
	//funcion para añadir los registros a la BD
	public function clienteRegistrado (Application $app, Request $request) {
	
	$_clave = $request->get('claveIngresado');
	$_nombre = $request->get('nombreIngresado');					// aqui se recuperan los datos ingresados
	$_apellido = $request->get('apellidoIngresado');					 
	$_correo = $request->get('correoIngresado');					 
	$_calle = $request->get('calleIngresado');
	$_colonia = $request->get('coloniaIngresado');
	$_cp = $request->get('zipIngresado');
	$_ciudad = $request->get('ciudadIngresado');
	$_pais = $request->get('paisIngresado');
	$_telefono = $request->get('telefonoIngresado');
	
		
    $app['db']->insert ('Clientes', array(						//aqui se añaden a la BD
        'CLAVE' => $_clave,
		'Nombre' => $_nombre,
		'Apellido' => $_apellido,
		'Correo' => $_correo,
		'Calle' => $_calle,
		'Colonia' => $_colonia,
		'CP' => $_cp,
		'CIUDAD' => $_ciudad,
		'PAIS' => $_pais,
		'TELEFONO' => $_telefono
      ));

	  return 'datos ingresados a la BD';
	
	} 

	//funcion para mostrar los productos: URL, categoria, producto, precio
	public function catalogoProductos (Application $app) {
		
		$sql_SKU = 'SELECT SKU FROM Productos';
		$fieldA = $app['db']->fetchAll($sql_SKU);
		foreach($fieldA as $posicion=>$codigo) {
			$sql_ITEM = "SELECT SKU,URL_IMAGEN_GRANDE,CATEGORIA_ID,NOMBRE,PRECIO FROM Productos WHERE SKU = '".$codigo['SKU']."' ";
			$fieldB = $app['db']->fetchAssoc($sql_ITEM);
			$sql_CATEGORIA = "SELECT NOMBRE_ FROM Categorias WHERE ID = '".$fieldB['CATEGORIA_ID']."' ";
			$fieldC = $app['db']->fetchAssoc($sql_CATEGORIA);
			
		if ($posicion == 0)	{$b0 = $fieldB; $c0 = $fieldC;}
		if ($posicion == 1)	{$b1 = $fieldB; $c1 = $fieldC;}
		if ($posicion == 2)	{$b2 = $fieldB; $c2 = $fieldC;}
 		if ($posicion == 3)	{$b3 = $fieldB; $c3 = $fieldC;}
		if ($posicion == 4)	{$b4 = $fieldB; $c4 = $fieldC;}
		if ($posicion == 5)	{$b5 = $fieldB; $c5 = $fieldC;}
		if ($posicion == 6)	{$b6 = $fieldB; $c6 = $fieldC;}
		if ($posicion == 7)	{$b7 = $fieldB; $c7 = $fieldC;}
		if ($posicion == 8)	{$b8 = $fieldB; $c8 = $fieldC;}
		if ($posicion == 9)	{$b9 = $fieldB; $c9 = $fieldC;}
		
		if ($posicion == 10)	{$b10 = $fieldB; $c10 = $fieldC;}
		if ($posicion == 11)	{$b11 = $fieldB; $c11 = $fieldC;}
		if ($posicion == 12)	{$b12 = $fieldB; $c12 = $fieldC;}
 		if ($posicion == 13)	{$b13 = $fieldB; $c13 = $fieldC;}
		if ($posicion == 14)	{$b14 = $fieldB; $c14 = $fieldC;}
		if ($posicion == 15)	{$b15 = $fieldB; $c15 = $fieldC;}
		if ($posicion == 16)	{$b16 = $fieldB; $c16 = $fieldC;}
		if ($posicion == 17)	{$b17 = $fieldB; $c17 = $fieldC;}
		if ($posicion == 18)	{$b18 = $fieldB; $c18 = $fieldC;}
		if ($posicion == 19)	{$b19 = $fieldB; $c19 = $fieldC;}
			
		}
						
		return $app['twig']->render('productos.twig', array(		
		   'b0' => $b0,	'c0' => $c0,	
		   'b1' => $b1,	'c1' => $c1,
		   'b2' => $b2, 'c2' => $c2,
		   'b3' => $b3,	'c3' => $c3,
		   'b4' => $b4,	'c4' => $c4,
		   'b5' => $b5,	'c5' => $c5,
		   'b6' => $b6,	'c6' => $c6,
		   'b7' => $b7,	'c7' => $c7,
		   'b8' => $b8,	'c8' => $c8,
		   'b9' => $b9,	'c9' => $c9,
		   
		   'b10' => $b10,	'c10' => $c10,	
		   'b11' => $b11,	'c11' => $c11,
		   'b12' => $b12, 	'c12' => $c12,
		   'b13' => $b13,	'c13' => $c13,
		   'b14' => $b14,	'c14' => $c14,
		   'b15' => $b15,	'c15' => $c15,
		   'b16' => $b16,	'c16' => $c16,
		   'b17' => $b17,	'c17' => $c17,
		   'b18' => $b18,	'c18' => $c18,
		   'b19' => $b19,	'c19' => $c19
		   		
		));
		
}

// funcion para enviar a la pantalla de login
public function login (Application $app) {

	return $app['twig']->render('login.twig');
	
}

//funcion para validar la sesion
public function loginValidacion (Application $app, Request $request) {

	$correo = $_SERVER['PHP_AUTH_USER'] = $request->get('user');      // se recuperan los datos introducidos por el usuario
	$clave = $_SERVER['PHP_AUTH_PW'] = $request->get('claveSesion');  // y se almacenan en variables $_SERVER
 
	$sql_clientes = 'SELECT Correo, CLAVE FROM Clientes';			 // se efectua una consulta de clientes a la BD	
	$registroCliente = $app['db']->fetchAll($sql_clientes); 
    
	foreach($registroCliente as $posicion1=>$codigo1) {				// se realiza la comparacion de los datos introducidos con los de la BD

		if ($codigo1['Correo'] === $correo && $codigo1['CLAVE'] === $clave) {       //si los datos coinciden se crea la sesion
      $app['session']->set('cliente_id', array('id' => $codigo1['CLAVE'])); 		// para el usuario
      // return new Response(json_encode($app['session']->get('cliente_id'))) ;
	   return $app->redirect('/example/web/productos');
    	}
	}
	
	return "El usuario {$correo} no esta registrado en la base de datos o ha introducido una contraseña incorrecta"; //error msg
    
} // fin de loginValidacion


public function intermedia (Application $app) {
	$sku = $_GET['sku']; 
	return $app->redirect("/example/web/producto/".$sku." ");
	
} 


public function producto (Application $app, $sku) {
	
	$sql_producto_seleccion = "SELECT SKU, URL_IMAGEN_GRANDE, NOMBRE, DESCRIPCION, PRECIO, CATEGORIA_ID FROM Productos
								WHERE SKU = '".$sku."' ";
	$productoSeleccionado = $app['db']->fetchAll($sql_producto_seleccion);

	foreach ($productoSeleccionado as $posicion2=>$codigo2){
		
			$sql_CATEGORIA_ = "SELECT NOMBRE_ FROM Categorias WHERE ID = '".$codigo2['CATEGORIA_ID']."' ";
			$psc = $app['db']->fetchAssoc($sql_CATEGORIA_);
		}
	
	
	return $app['twig']->render('seleccion.twig', array(
	'ps' => $codigo2,
	'psc' => $psc
	));	

} // fin de clase producto

public function intermedia2 (Application $app) {
	$skus = $_GET['skus']; 
	return $app['twig']->render('agregar.twig', array( 
	'skus' => $skus));
	
}

public function agregar (Application $app , Request $request) {
	$_skus = $request->get('skus');
	$_cardnumber = $request->get('cardnumber');
	$_nameTarjeta = $request->get('nameTarjeta');
	$_fechaVencimiento = $request->get('fechaVencimiento');
	
	$usuarioCode = json_encode($app['session']->get('cliente_id'));
	$usuarioNumber = json_decode($usuarioCode)->id;
		
	$app['db']->insert ('Ordenes', array(						
        'CLIENTE_ID' => $usuarioNumber,
		'METODO_PAGO' => 'Tarjeta',
		'NUMERO_TARJETA' => $_cardnumber,
		'NOMBRE_TARJETAHABIENTE' => $_nameTarjeta,
		'FECHA_VENCIMIENTO' => $_fechaVencimiento
	 ));
	
	$sql_ORDEN_ID = "SELECT MAX(ID) as id FROM Ordenes"; 
	$ORDEN_ID = $app['db']->fetchAll($sql_ORDEN_ID);
	
	 foreach ($ORDEN_ID as $posicion4=>$codigo4){
	 }
		
	$app['db']->insert ('Pedidos', array(						
        'ORDEN_ID' => $codigo4['id']
	 ));
	    
	return $app->redirect("/example/web/carrito/".$_skus." ");
	
} // fin de agregar

public function carrito (Application $app, $_skus) {
	
	$sql_producto_carrito = "SELECT URL_IMAGEN, NOMBRE, PRECIO FROM Productos
								WHERE SKU = '".$_skus."' ";
	$productoCarrito = $app['db']->fetchAssoc($sql_producto_carrito);
		
	return $app['twig']->render('carrito.twig', array(
	'skus' => $_skus,
	'item' => $productoCarrito
	));	
} // fin de carrito

public function carritoPOST (Application $app, Request $request) {

	$usuarioCode = json_encode($app['session']->get('cliente_id'));
	$usuarioNumber = json_decode($usuarioCode)->id;
	

$_skus = $request->get('skus');
$_cantidad = $request->get('cantidad');
$_url = $request->get('url');
$_nombre = $request->get('nombre');
$_precio = $request->get('precio');

	$app['db']->insert ('Carrito', array(						
        'CLIENTE_ID' => $usuarioNumber,
		'SKU' => $_skus,
		'CANTIDAD' => $_cantidad			
	 ));
	

$subtotal = $_cantidad*$_precio;

return $app['twig']->render('carritoact.twig', array(
	'url' => $_url,
	'nombre' => $_nombre,
	'cantidad' => $_cantidad,
	'precio' => $_precio,
	'subtotal' => $subtotal
	));	
} // fin de carritoPOST


public function checkout (Application $app, Request $request) {

	$sql_ORDEN = "SELECT MAX(ID) as id FROM Ordenes"; 
	$ORDEN = $app['db']->fetchAssoc($sql_ORDEN);

	$sql_registro = "SELECT NOMBRE_TARJETAHABIENTE, NUMERO_TARJETA, FECHA_VENCIMIENTO FROM Ordenes WHERE ID = '".$ORDEN['id']."' ";
	$registro = $app['db']->fetchAssoc($sql_registro);

	$CVC = '123';
	
	$_url = $request->get('url');
	$_nombre = $request->get('nombre');
	$_cantidad = $request->get('cantidad');
	$_precio = $request->get('precio');
	$_subtotal = $request->get('subtotal');

	$date = date_create($registro['FECHA_VENCIMIENTO']);
	$fecha=date_format($date, 'd-m-Y');
	
	return $app['twig']->render('checkout.twig', array(
	
	'NTH' => $registro['NOMBRE_TARJETAHABIENTE'],
	'NTC' => $registro['NUMERO_TARJETA'],
	'CVC' => $CVC,
	'FE' => $fecha,
	'url' => $_url,
	'nombre' => $_nombre,
	'cantidad' => $_cantidad,
	'precio' => $_precio,
	'subtotal' => $_subtotal
	));
	
} // fin de checkout

public function finalizarCompra (Application $app, Request $request){
	$usuarioCode = json_encode($app['session']->get('cliente_id'));
	session_destroy();
	return 'Sesion finalizada -- GRACIAS POR SU COMPRA';
	} // fin de finalizarCompra

} // fin de clase FrontEnd


	
	
